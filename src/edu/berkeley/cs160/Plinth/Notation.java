package edu.berkeley.cs160.Plinth;

import edu.berkeley.cs160.Plinth.GameActivity.BUTTON_TYPE;

class Notation {
	private int tick;
	private BUTTON_TYPE button;
	private long glowTime;
	
	Notation(int tick, BUTTON_TYPE button, long glowTime) {
		super();
		this.tick = tick;
		this.button = button;
		this.glowTime = glowTime;
	}
	
	protected int getTick() {
		return tick;
	}

	protected BUTTON_TYPE getButton() {
		return button;
	}
	
	protected long getGlowTime() {
		return glowTime;
	}
}
