package edu.berkeley.cs160.Plinth;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Chronometer;
import android.widget.TextView;
import android.graphics.drawable.Drawable;
import java.util.LinkedList;
import edu.berkeley.cs160.Plinth.R;

public class GameActivity extends Activity {
	private Drawable finger_off, finger_on, anticipate;
	private ImageView buttonA, buttonB, buttonC, buttonD, buttonE;
	private MediaPlayer soundA, soundB, soundC, soundD, soundE;
	private Chronometer chrono;
	private Button pauseButton;
	private TextView scoreView;
	private int tickNumber = 0;
	private LinkedList<Notation> notes;
	public int score = 0;
	private Resources resources;
	private Intent scoreIntent;
	private boolean paused;
	
	public long NOTE_LENGTH = 10000;
	public static final long NUM_TICKS = 64;
	
	static final int SCORE_SCREEN = 0;
	static final int GAME_EXIT = 0;
	static final int GAME_RESTART = 1;
	
	private static final int CORRECT_SCORE = 5;
	private static final int INCORRECT_SCORE = -2;
	
	private long startTime, period, pauseTime;
	
	public static final long ROUND_TIME = 1 * 30 * 1000; // 30 seconds
	
	private Handler loopHandler = new Handler();
	private Handler resetHandler = new Handler();
	
	public enum BUTTON_TYPE {
		A, B, C, D, E
	}
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);
        
        //Log.e("DUDE", "Started");
        
        resources = getResources();

        Bundle extras = getIntent().getExtras();
        int rhythm = extras.getInt("rhythm");
        NOTE_LENGTH = extras.getInt("notelength");
        
        this.fillNotes(rhythm);
		buttonA = (ImageView) findViewById(R.id.buttonA);
		soundA = MediaPlayer.create(this.getApplicationContext(), R.raw.s1);
		buttonB = (ImageView) findViewById(R.id.buttonB);
		soundB = MediaPlayer.create(this.getApplicationContext(), R.raw.s2);
		buttonC = (ImageView) findViewById(R.id.buttonC);
		soundC = MediaPlayer.create(this.getApplicationContext(), R.raw.s3);
		buttonD = (ImageView) findViewById(R.id.buttonD);
		soundD = MediaPlayer.create(this.getApplicationContext(), R.raw.s4);
		buttonE = (ImageView) findViewById(R.id.buttonE);
		soundE = MediaPlayer.create(this.getApplicationContext(), R.raw.s5);
		finger_off = resources.getDrawable(R.drawable.finger_off);
		finger_on = resources.getDrawable(R.drawable.finger_on_border);
		anticipate = resources.getDrawable(R.drawable.anticipate);
		chrono = (Chronometer) findViewById(R.id.t_time);
		scoreView = (TextView) findViewById(R.id.t_score);
		pauseButton = (Button) findViewById(R.id.buttonPause);
		
        period = NOTE_LENGTH / NUM_TICKS;
        
        //Log.v("DUDE", "Period: " + String.valueOf(period));
        scoreIntent = new Intent(this, ScoreActivity.class);
        reset();
    }
    
	private void fillNotes(int rhythm) {
		// Lame helper method generating a sample rhythm
		this.notes = new LinkedList<Notation>();
		switch (rhythm) {
		case 0:
			this.notes.add(new Notation(0, BUTTON_TYPE.B, NOTE_LENGTH / 16));
			this.notes.add(new Notation(8, BUTTON_TYPE.B, NOTE_LENGTH / 16));
			this.notes.add(new Notation(16, BUTTON_TYPE.D, NOTE_LENGTH / 8));
			this.notes.add(new Notation(32, BUTTON_TYPE.E, NOTE_LENGTH / 32));
			this.notes.add(new Notation(36, BUTTON_TYPE.D, NOTE_LENGTH / 32));
			this.notes.add(new Notation(40, BUTTON_TYPE.C, NOTE_LENGTH / 16));
			this.notes.add(new Notation(48, BUTTON_TYPE.A, NOTE_LENGTH / 8));
			break;
		case 1:
		default:
			this.notes.add(new Notation(0, BUTTON_TYPE.A, NOTE_LENGTH / 32));
			this.notes.add(new Notation(4, BUTTON_TYPE.A, NOTE_LENGTH / 32));
			this.notes.add(new Notation(8, BUTTON_TYPE.E, NOTE_LENGTH / 16));
			this.notes.add(new Notation(16, BUTTON_TYPE.B, NOTE_LENGTH / 64));
			this.notes.add(new Notation(18, BUTTON_TYPE.B, NOTE_LENGTH / 64));
			this.notes.add(new Notation(20, BUTTON_TYPE.B, NOTE_LENGTH / 32));
			this.notes.add(new Notation(24, BUTTON_TYPE.E, NOTE_LENGTH / 16));
			this.notes.add(new Notation(32, BUTTON_TYPE.E, NOTE_LENGTH / 64));
			this.notes.add(new Notation(34, BUTTON_TYPE.A, NOTE_LENGTH / 32));
			this.notes.add(new Notation(38, BUTTON_TYPE.A, NOTE_LENGTH / 32));
			this.notes.add(new Notation(42, BUTTON_TYPE.E, NOTE_LENGTH / 64));
			this.notes.add(new Notation(44, BUTTON_TYPE.D, NOTE_LENGTH / 64));
			this.notes.add(new Notation(46, BUTTON_TYPE.C, NOTE_LENGTH / 8));
			break;
		}
	}
	
	private void reset() {
		score = 0;
		scoreView.setTextColor(resources.getColor(R.color.white));
		scoreView.setText(String.valueOf(score));
		
		tickNumber = 0;
		
		buttonA.setImageDrawable(finger_off);
		buttonB.setImageDrawable(finger_off);
		buttonC.setImageDrawable(finger_off);
		buttonD.setImageDrawable(finger_off);
		buttonE.setImageDrawable(finger_off);
		pauseButton.setText("Pause");
		paused = false;
		
		loopHandler.removeCallbacks(tickTask);
        loopHandler.postDelayed(tickTask, period);
        startTime = System.currentTimeMillis();
        chrono.setBase(android.os.SystemClock.elapsedRealtime());
        chrono.start();
	}
    
	protected void toggleButton(BUTTON_TYPE id, boolean value) {
		ImageView button;
		Drawable src;
		
		if (value) {
			src = this.finger_on;
		} else {
			src = this.finger_off;
		}
		
		switch(id) {
		case A:
			button = this.buttonA;
			break;
		case B:
			button = this.buttonB;
			break;
		case C:
			button = this.buttonC;
			break;
		case D:
			button = this.buttonD;
			break;
		case E:
		default:
			button = this.buttonE;
			break;
		}
		
		button.setImageDrawable(src);
	}
	
	protected void toggleAnticipate(BUTTON_TYPE id) {
		ImageView button;
		Drawable src;
		
		src = this.anticipate;
		
		switch(id) {
		case A:
			button = this.buttonA;
			break;
		case B:
			button = this.buttonB;
			break;
		case C:
			button = this.buttonC;
			break;
		case D:
			button = this.buttonD;
			break;
		case E:
		default:
			button = this.buttonE;
			break;
		}
		
		button.setImageDrawable(src);
	}
	
	private Runnable tickTask = new Runnable() {
		public void run() {
			//final long start = System.currentTimeMillis();
 	       
 	       	if (paused) {
 	       		loopHandler.postDelayed(this, period);
 	    	   return;
 	       	}
 	       	
			if (tickNumber >= NUM_TICKS) {
				tickNumber = 0; // Restart loop
 	       	}
 	       
			//Log.v("DUDE", "Starting tick #" + String.valueOf(tickNumber));
 			
			// Activate buttons for this tick (if any)
 	       	for (Notation np : notes) {
 	       		if (np.getTick() == tickNumber) {
 	       			toggleButton(np.getButton(), true);
 	       			resetHandler.postDelayed(genToggleTask(np.getButton()), np.getGlowTime());
 	       		}
 	       		if (np.getTick() == tickNumber+2 % NUM_TICKS) {
 	       			toggleAnticipate(np.getButton());
 	       		}
 	       	}
 			
 	       	// Increment tick count
 	       	tickNumber++;
 	       	
 	       	final long currentTime = System.currentTimeMillis();
 	       	
 	       	if ((currentTime - startTime) < ROUND_TIME) {
 	       		loopHandler.postDelayed(this, period);
 	       	} else {
 	       		// Round ended
 	       		chrono.stop();
 	       		scoreIntent.putExtra("score", score);
 	       		startActivityForResult(scoreIntent, SCORE_SCREEN);
 	       	}
		}
	};
    
    private ToggleTask genToggleTask(BUTTON_TYPE button) {
    	return new ToggleTask(button, this);
    }
    
    public void onExitClick(View view) {
    	this.finish();
    }
    
    public void onButtonClick(View view) {
    	ImageView button = (ImageView) view; 
    	
    	//Log.v("DUDE", "button.getDrawable() = " + button.getDrawable().toString() + ", finger_off = " + finger_off.toString());
    	
    	if (button == buttonA) {
    		soundA.start();
    	} else if (button == buttonB) {
    		soundB.start();
    	} else if (button == buttonC) {
    		soundC.start();
    	} else if (button == buttonD) {
    		soundD.start();
    	} else if (button == buttonE) {
    		soundE.start();
    	}
    	
    	if (!paused) {
    		if (button.getDrawable() == finger_off) {
    			// if not clickable
    			updateScore(INCORRECT_SCORE);
    		} else {
    			updateScore(CORRECT_SCORE);
    		}
    	}
    }
    
    public void onPauseClick(View view) {
    	if (paused) {
    		pauseButton.setText("Pause");
    		paused = false;
    		startTime = startTime + pauseTime;
    		chrono.start();
    	} else {
    		pauseButton.setText("Resume");
    		paused = true;
    		pauseTime = System.currentTimeMillis() - startTime;
    		chrono.stop();
    	}
    }
    
    protected void onActivityResult(int requestCode, int resultCode,
            Intent data) {
        if (requestCode == SCORE_SCREEN) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                if (extras.getInt("result") == GAME_EXIT) {
                	finish();
                } else {
                	reset();
                }
            }
        }
    }

    private void updateScore(int change) {
    	score += change;
    	
    	if (score > 0) {
    		scoreView.setTextColor(resources.getColor(R.color.blue));
    	} else if (score < 0) {
    		scoreView.setTextColor(resources.getColor(R.color.red));
    	} else {
    		scoreView.setTextColor(resources.getColor(R.color.white));
    	}
    	
    	scoreView.setText(String.valueOf(score));
    }
};