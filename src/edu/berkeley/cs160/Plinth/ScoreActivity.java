package edu.berkeley.cs160.Plinth;

import edu.berkeley.cs160.Plinth.R;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ScoreActivity extends Activity {
	Resources resources;
	Button againButton, menuButton;
	TextView scoreView;
	private int score;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.score);
        
        againButton = (Button) this.findViewById(R.id.againButton);
        menuButton = (Button) this.findViewById(R.id.menuButton);
        scoreView = (TextView) this.findViewById(R.id.t_score);
        
        resources = getResources();
        Bundle extras = getIntent().getExtras();
        score = extras.getInt("score");
        
        if (score > 0) {
    		scoreView.setTextColor(resources.getColor(R.color.blue));
    	} else if (score < 0) {
    		scoreView.setTextColor(resources.getColor(R.color.red));
    	} else {
    		scoreView.setTextColor(resources.getColor(R.color.white));
    	}
        scoreView.setText(String.valueOf(score));
    }
    
    public void onClickButton(View view) {
    	Button button = (Button) view;
    	Intent i = new Intent();
    	
    	if (button == againButton) {
    		i.putExtra("result", GameActivity.GAME_RESTART);
    	} else {
    		i.putExtra("result", GameActivity.GAME_EXIT);
    	}
    	
    	setResult(RESULT_OK, i);
    	finish();
    }
}
