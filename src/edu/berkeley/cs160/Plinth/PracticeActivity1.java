package edu.berkeley.cs160.Plinth;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.Intent;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.ArrayAdapter;

public class PracticeActivity1 extends Activity {
	 /** Called when the activity is first created. */
	 public void onCreate(Bundle savedInstanceState) {
	    	super.onCreate(savedInstanceState);
	        setContentView(R.layout.practice1);
	        
	        Button next = (Button) findViewById(R.id.button1);
	        Button back = (Button) findViewById(R.id.button2);
	        Spinner s = (Spinner) findViewById(R.id.spinner1);
	        ArrayAdapter adapter = ArrayAdapter.createFromResource(
	        		this, R.array.handoption, android.R.layout.simple_spinner_item);
	        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	        s.setAdapter(adapter);
	        
	        next.setOnClickListener(new View.OnClickListener() {
	        	public void onClick(View view){
	        		Intent myIntent = new Intent(PracticeActivity1.this, PracticeActivity2.class);
	        		startActivity(myIntent);
	        	}
	        });
	        
	        back.setOnClickListener(new OnClickListener() {     
	        	public void onClick(View v) {
	        		finish();	
	        	}
	        });       
	 }
}
