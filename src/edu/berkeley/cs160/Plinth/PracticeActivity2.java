package edu.berkeley.cs160.Plinth;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.Button;
import android.media.MediaPlayer;
//import android.view.View;
import android.view.View.OnClickListener;

public class PracticeActivity2 extends Activity {
    private static final int DOTS_INVISIBILITY = 4000;
	private static final int DOT3_VISIBILITY = 3500;
	private static final int DOT2_VISIBILITY = 3000;
	private static final int DOT1_VISIBILITY = 2000;
	
	/** Called when the activity is first created. */
	MediaPlayer mediaPlayer;
    @Override
    
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.practice2);
        Button button1 = (Button) findViewById(R.id.button1);
        Button button2 = (Button) findViewById(R.id.button2);
        Button button3 = (Button) findViewById(R.id.button3);
        
        mediaPlayer = MediaPlayer.create(getBaseContext(), R.raw.beat);

        button1.setOnClickListener(new OnClickListener(){      
        	public void onClick(View v){	
        		finish();
        	}
        });
        
        button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				playSound();
			}    	
        });
        
        button3.setOnClickListener(new OnClickListener(){
            ImageView dot1 = (ImageView) findViewById(R.id.imageView2);
            ImageView dot2 = (ImageView) findViewById(R.id.imageView3);
            ImageView dot3 = (ImageView) findViewById(R.id.imageView4);

            @Override
			public void onClick(View v) {
            	Handler mHandler = new Handler();
            	mHandler.postDelayed(new Runnable(){
            		public void run(){
            			dot1.setVisibility(View.VISIBLE);
            		}
            	}, DOT1_VISIBILITY);
            	
            	mHandler.postDelayed(new Runnable(){
            		public void run(){
            			dot2.setVisibility(View.VISIBLE);
            		}
            	}, DOT2_VISIBILITY);
            	
            	mHandler.postDelayed(new Runnable(){
            		public void run(){
            			dot3.setVisibility(View.VISIBLE);
            		}
            	}, DOT3_VISIBILITY);
            	
            	mHandler.postDelayed(new Runnable(){
            		public void run(){
            			dot1.setVisibility(View.INVISIBLE);
            			dot2.setVisibility(View.INVISIBLE);
            			dot3.setVisibility(View.INVISIBLE);
            		}
            	}, DOTS_INVISIBILITY);	
			}
        });
    }   

    private void playSound() {
    	try {
    		mediaPlayer.stop();
    		mediaPlayer.prepare();
    		mediaPlayer.start();
    	} catch (Exception e){
    		
    	}
    }
}

