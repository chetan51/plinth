package edu.berkeley.cs160.Plinth;

import edu.berkeley.cs160.Plinth.GameActivity.BUTTON_TYPE;

public class ToggleTask implements Runnable {
	private GameActivity.BUTTON_TYPE button;
	private GameActivity activity;
		
	public ToggleTask(BUTTON_TYPE button, GameActivity activity) {
		super();
		this.button = button;
		this.activity = activity;
	}

	@Override
	public void run() {
		activity.toggleButton(button, false);
	}
}
