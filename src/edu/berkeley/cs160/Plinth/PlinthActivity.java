package edu.berkeley.cs160.Plinth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.*;
import android.view.*;

public class PlinthActivity extends Activity {
	Intent gameIntent, practiceIntent;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Spinner level_spinner = (Spinner) findViewById(R.id.level_dropdown);
        ArrayAdapter<CharSequence> level_adapter = ArrayAdapter.createFromResource(
                this, R.array.levels_array, android.R.layout.simple_spinner_item);
        level_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        level_spinner.setAdapter(level_adapter);
        
        Spinner rhythm_spinner = (Spinner) findViewById(R.id.rhythm_dropdown);
        ArrayAdapter<CharSequence> rhythm_adapter = ArrayAdapter.createFromResource(
                this, R.array.rhythms_array, android.R.layout.simple_spinner_item);
        rhythm_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rhythm_spinner.setAdapter(rhythm_adapter);
        
        final ViewFlipper viewFlipper = (ViewFlipper)findViewById(R.id.viewFlipper);
        
        Button optionsButton = (Button) this.findViewById(R.id.optionsButton);
        optionsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	viewFlipper.setInAnimation(PlinthActivity.this, android.R.anim.fade_in);
            	viewFlipper.setOutAnimation(PlinthActivity.this, android.R.anim.fade_out);
            	viewFlipper.showNext();
            }
        });
        
        Button mainMenuButton = (Button) this.findViewById(R.id.mainMenuButton);
        mainMenuButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	viewFlipper.setInAnimation(PlinthActivity.this, android.R.anim.fade_in);
            	viewFlipper.setOutAnimation(PlinthActivity.this, android.R.anim.fade_out);
            	viewFlipper.showPrevious();
            }
        });
        
        gameIntent = new Intent(this, GameActivity.class);
        gameIntent.putExtra("rhythm", rhythm_spinner.getSelectedItemPosition());
        int notelen;
        switch (level_spinner.getSelectedItemPosition()) {
        case 0:
        	notelen = 10000;
        	break;
        case 1:
        	notelen = 7500;
        	break;
        case 2: 
        default:
        	notelen = 5000;
        	break;
        }
        gameIntent.putExtra("notelength", notelen);
        practiceIntent = new Intent(this, PracticeActivity1.class);

        Button playButton = (Button) this.findViewById(R.id.button3);
        playButton.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        		startActivity(gameIntent);
        	}
        });
        
        Button exitButton = (Button) this.findViewById(R.id.button2);
        exitButton.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        		finish();
        	}
        });
        
        Button practiceButton = (Button) this.findViewById(R.id.button1);
        practiceButton.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        		startActivity(practiceIntent);
        	}
        });
    }
}